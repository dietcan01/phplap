<?php
    // tạo ra 1 lớp
    class animal 
    {
        public $ten;
        public $hanhdong;
    }
    // tạo ra dối tượng dog
    $dog = new animal();

    // Gán các giá trị cho con chó
    $dog->ten = "MIC KI";
    $dog->hanhdong = "Vẫy đuôi";

    // kiểm tra
    echo $dog->ten;
    echo $dog->hanhdong;
    echo "</br>";
?> 
 <!-- Hành động  -->
 <?php
    class sieunhan 
    {
        // các thuộc tính
        public $ten;
        public $mausac;
        public $sucmanh;

        // tạo hành động
        function sayhi()
        {
            return " Tôi là siêu nhân";
        }

        function thuoctinh()
        {
            // $this để người dùng truy vẫn đến thuộc tính
            $test = $this->ten; 
            $test .= $this->mausac; 
            $test .= $this->sucmanh;

            return $test;
        }
    }

    // Gán các thuộc tính
    $ironMan = new sieunhan();
    $ironMan->ten = "David";
    $ironMan->mausac = "red";
    $ironMan->sucmanh = 100;
   

    echo $ironMan->thuoctinh();
    echo $ironMan->sayhi();
    echo "</br>";
 ?>
 <!-- Hàm rút gọn -->
 <?php 
  class Superman 
  {
    public $ten ;
    public $sucmanh ;

    function giatri($a,$b)
    {
      $this->ten = $a;
      $this->sucmanh= $b;
    }
   
  }
  $Sngao = new Superman();
  $Sngao->giatri("Gao đổ",45);
  echo $Sngao->ten;
  echo $Sngao->sucmanh;
 ?>
<!-- Hàm constructor -->
<?php
  class dongVat
  {
    public $ten;
    public $loai;

    function dongVat ($xp,$vd)
    {
        $this->ten = $xp;
        $this->loai = $vd;
    }
  }

  $conmeo = new dongVat("BABY","Tam thể");

  echo $conmeo->ten;
  echo $conmeo->loai;
?>
<!-- Tính thừa kế -->