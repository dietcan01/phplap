<?php
    include"connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <?php
        if(isset($_POST['submit'])){

            $title = $_POST['names'];

            if(isset($_FILES['img'])){

                // chỉ lấy ảnh lên database
                $img = $_FILES['img']['name'];

                // lấy đường dẫn của ảnh
                $name_img = $_FILES['img']['tmp_name'];
            }
           
            $price = $_POST['price'];

            $insurance = $_POST['insurance'];

            $sql = " INSERT INTO product ( name , images , price , insurance )
            VALUES('$title', '$img' , '$price' , '$insurance') ";

            if($conn->query($sql) == TRUE){
                echo"<script> alert('Thêm dữ liệu thành công'); </script>";
            }else{
                echo"<script> alert('Thêm dữ liệu không thành công'); </script>";
            }
                }
    ?>
    <div class="container">
        <form action="add_product.php" method="post" enctype="multipart/form-data">
            <h3 class="text-center py-4">Thêm sản phẩm</h3>
            <div class="col py-4">
                <input type="text" class="form-control" name="names" placeholder="Tên sản phẩm" >
            </div>
            <div class="col pb-4">
                <input type="file" class="form-control" name="img">
            </div>
            <div class="row g-3">
                <div class="col">
                    <input type="text" class="form-control" name="price" placeholder="Nhập giá tiền" >
                </div>
                <div class="col">
                    <input type="text" class="form-control" name="insurance" placeholder="Nhập bảo hành">
                </div>
            </div>
            <input class="btn btn-info my-4 " type="submit" name="submit" value="thêm sản phẩm">
        </form>
        <a href="product.php">đến trang product</a>
    </div>
</body>
</html>