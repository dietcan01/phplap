<?php
    include"connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>hqshop</title>
</head>
<style>
    body{
        font-family: sans-serif;
    }
    a{
        text-decoration: none;
    }
    .text{
        width: 100%;
        padding: 0 10px;
        overflow: hidden;
        text-overflow: ellipsis;
        line-height: 25px;
        -webkit-line-clamp: 2;
        display: -webkit-box;
        -webkit-box-orient: vertical;
    }
    .img{
        width: 100%;
        height: 300px;
        object-fit: cover;
    }
    span{
        font-size: 1.2rem;
    }
</style>
<body>
    <div class="container">
        <table border="1" class="my-2">
            <tr>
                <th rowspan="3">Chức Năng : </th>
                <td><a href="add_product.php">Thêm sản phẩm</a></td>
            </tr>
        </table>
        <h1 class="text-center py-5">product</h1>
        <div class="row">
            <?php
                $sql = "SELECT * FROM product";
                $result = mysqli_query($conn,$sql);
                while($row = mysqli_fetch_assoc($result)){
            ?>
                <div class="col-12 col-md-3 py-4">
                    <div class="border">
                        <img class="img" src="img/<?php echo $row['images'] ?>" alt="">
                        <span><?php $row['id'] ?></span>
                        <h5 class="mt-3 mb-2 text"><?php echo $row['name'] ?></h5>
                        <div class="price text-center py-1">
                            <span class=" fw-bold"><?php echo $row['price'] ?>đ </span>
                            <span class=" fw-bold">Bảo hành:<?php echo $row['insurance'] ?></span>
                        </div>
                        <div class="content py-2 d-flex justify-content-around">
                            <a class="btn btn-danger px-5" href="delete.php?this_id=<?php echo $row['id'] ?>">Xóa</a>
                            <a class="btn btn-success px-5" href="edit.php?this_id=<?php echo $row['id'] ?>">Sửa</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</body>
</html>