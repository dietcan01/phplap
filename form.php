<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="main.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <!-- bài 4.1 -->
<?php
    $list = [
        [
            "image" => "https://media.worldnomads.com/explore/vietnam/halong-bay-vietnam-from-above-gettyimages.jpg",
            "title" => "Hình ảnh 1"
        ],
        [
            "image" => "https://www.visithalongbay.com/media/cache/1a/f5/1af56f83eb2ba43b31351ae2e0cb1b7b.jpg",
            "title" => "Hình ảnh 2"
        ],
        [
            "image" => "https://bcp.cdnchinhphu.vn/Uploaded_VGP/phamvanthua/20190725/0052583f41011fc2c2f516e5b4b693ec%20(1).jpg",
            "title" => "Hình ảnh 3"
        ],
        [
            "image" => "https://vcdn-english.vnecdn.net/2019/07/24/10dayVietnam4-1563961581-4041-1563961635_1200x0.jpg",
            "title" => "Hình ảnh 4"
        ]
    ]
?>
    <?php
        if(isset($_POST["submitt"])){
            if(!empty($_POST['chon'])){
                foreach($_POST['chon'] as $selected){
                }
            }
        }
    ?>
    <div class="container pt-5">
        <h3 class="text-center">Form select lap</h3>
        <form action="" method="post">
            <select id="" name="chon[]" class="drop form-control">
                <option value="" disabled selected>Chọn ảnh</option>
                <?php foreach ($list as $value){ ?>
                    <option  value=" <?php echo $value['image'] ?>" class="" ><?php echo $value['title'] ?> </option>
                <?php } ?>
            </select>
            <input class="btn btn-primary mt-3" type="submit" name="submitt" value="Xem ảnh">
        </form>
        <div class="content py-4">
            <img class="w-100" src="<?php echo $selected;?> "/>
        </div>
    </div>
    
    <!-- bài 4.2 -->
    <?php
        $row = "1";
       if(isset($_POST['row'])){
            $row = $_POST['row'];
       }
    ?>
    <div class="container">
        <h3 class="text-center py-2">Form tạo sản phẩm</h3>
        <form action="" method="post">
            <input name="row" type="text" placeholder="Nhập số lượng...">
            <input class="btn btn-primary" type="submit" value="submit">
        </form>
        <div class="row py-5">
            <?php for($i = 0; $i < $row; $i++ ){?>
                    <?php if($i % 2 != 0) {?>
                        <div class="col-12 col-md-3 py-3">
                        <div class="border">
                            <img class="w-100 hq" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQDxAPEBIQFRAPDQ8QDw8NDw8PDhAPFREXFxYRFRMYHSggGBolGxMVITEhJSk3MC4uFx8zODMsNygtLisBCgoKDg0OGBAQGC0lIB0tLS0tKzAvLSsrLS0tNi0tLy0tLy0tLS4tKy8tLS0tKy0tLS0tLS0rLS0tLS0tLS0tLf/AABEIAKwBJgMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAgMBBAUGB//EADoQAAICAQMDAgMHAgILAQAAAAABAgMRBBIhBRMxQWEGUXEUIkKBkaGxMtEVgiQlM0NSU5PBwuLwB//EABoBAQADAQEBAAAAAAAAAAAAAAABAgMFBAb/xAAyEQEAAgIABAIIBgIDAQAAAAAAAQIDEQQSITETUQVBYXGBkaHBIjJCUrHhFCNy0fAk/9oADAMBAAIRAxEAPwD4aAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAykBLaSM7QG0DOwaDYNBsGhnYNB2xoO2NB2xoO2NB2wHbGg7Y0HbGhjYNBsGg2DQbBoVEAAAAAAAAAAAAAAAAAAAMxYF8YlkJqAGNoGdoE1AaGe2NAqxoZUCdCaqGhnsjQx2RoOyNA6RoY7Q0MdoaDtDQx2hoO2RoY7Y0NAqkAAAAAAAAAAAAAAAAAAADYol6EwiWwkWQk4gRSAkuAJJkibS8khWstoC2EBoWKBOhJVE6GXSRoOwToRdA0IukjQw6RoR7Q0MOsjQi4DQ4xksAAAAAAAAAAAAAAAAAAABmLwBvVWJrk0iVViePoSKrLUVmRU5shIpMCUckoWYkufkSNmi94eS0SNuqafktCG1XEkXOskTjShoYlQNCDpGhXKojQh2hoVzqI0K3WNDzh51wAAAAAAAAAAAAAAAAAAAMgWVzwTEoTlP5MnYhkgZTJFsKpPnBMRIsVUl4/QnUoS3SXn9yesCdEJTf3Yv6omtbW7QzvkrTvLe+z2J4lB+OMfyaTS8d4UjiMU/qbMHtxnj6r9yO3deL1tHSW9U8loS2YwLCSrJ0jaM6sEaNqJ1kaTCPaI0bRlUNJmVUqhpG3jTyNQAAAyot+E+POPQDAAAAAAAAAAAAAAMgAMoDKJG3pdJu+eC9a7VmXRjpYx8L8zSKxCu2XALRCE1hZITLWoqdsvOEK055Y5cnI9N0rQOHDSafrHyjpYMPK4vE5+Z6XQ6CM8NrD90dKmKJjcuNl4i1ekOjb0imWVJRfHqWthx2jrV568Vmp1rZw+o/DzrzOvmOc7fkc3iOB5euP5O9wXpjm1TL83MSlH0+q9TwO7ExMbhs1STRaBZtySKJ18kEI9shKEqwKZQI0PBnjbL9FUp21wecTshF484cknj9SaxuYhW06iZfUqfgnQR81yl7ztn/4tI7tfR+GPOXzNvSvET2mI+CzoHR9Mq3LsU7lqNVFSlXGUko6myMVl/JJL8ieG4fFy75Y7z/MnGcVm59Redar/ABC74pio6DUpJJdl8RSS8ovxdYjDbUM+Bva3E03L5Z0Tpv2m7s79spQm4Zju3TjFyUPKxnD5ODix+Jbl2+nz5fCrza8l3w90SWsnOEZbdlTnlx3JyylGHleWy2HDOWZiPUpxHERhrEzHedf38FPTOm96Oom5bY6eiVrbjndLKUa/Kw2359itMfNFp8o2vky8k1jXW06U9QhUrGqJTlXiOJWRUZZ2rPC98lb8u/w9l6c2vxR1a8oNcNNfVYKrOh0Dpy1GorqnujGxWPdFc/drlLjPHmKNcOPnvFZ9bHPl8Ok2j1a+rnxg34Tfrws8GTaWMAMAZjBvwm+M8Jvj5gb/AEfpM9TJpSrhGLip2WyUUtzwkl5k3h4SNMeKbzremWbNGON6mfZDcfQoQlqJXW7KNPqHQrFXvsttTf3YwzxwsvL448mngxHNzT0ide+Wf+RNoryV3No3ryj3tTq/Tfs8oYnvqtrjbVbGLjug+OYvw008opkx8kx13E9Ylpiy+JE9NTE6mGj9H+qwZtUSBZV5JhDt6KaaPRVnK+McsJqtVJDWIaHVVtjj5lbdlZ7tjpWmjtTaTaf4m0erDSNOZxGSeaY29RROEIpp8t4ST3fydOsxWOji2i17alrfEXxBOiuMYN75cYkk8Y9TLi+MtjrqveW3A+j65rza3aHl9L8RaiL+9ZJxys8v9jl043LWesuzk9HYLR0rD6R8M9W79eJ84fu8ryn7He4fN4td+t8px3DRgv0S6105Y3xX1xhHn4rh4tHNXu93ozj5rMUtPR5qxbHlHMfS9+zbosUlwW2hKUQlHaBVYiBrtAfPzwt210vPfpx571eHjOHvXoWp+aFb/ll9ndN//Nh/0P8A3PpOXJ+6Pl/b4/nw/sn5/wBOb0em91PbdBL7Tq+HRu5+02Zed/zyzDBXJydLeufV7Zevir4ef8VJnpX1+yPYp+J6b1otQ5XQce08xVG1tZXGd7wV4quSMVt2+ieCyYZz1itJiff/AE+YdL1jovquX+7sjPj1SfK/NZRxcd+S0W8n0eWkZKTSfXD13U2un9ycPOo6lC2GPXTV4tSXtmxL8j2X/wBO5j123Hujr93Pxf8A0ai36a6n3z0+yj4k08dLRqYxx/puuUoe+njFWLH+axL8iM9Yx0tEfqn6d/utw9py3rM/or1989Ps3tZe6rup2Rxuho9G4tpNKWK8Sw/VeV7o0vPLbJMeUfZlSOauGJ/db7tPp0vttGkeqlva6qqd8+Zup1qfbcvVNr9zOn+2tOfr+LXwaZP9N7+HGvwb17fNZ0LrGps6jKqbbrT1C7TitlKhCe3avw44Wff3LYc17ZtT269PJHEYcdeH5o79Ovn1jv5udqtbZpdJolppuCvrsttsrwpWWKxx2uXnEUkse5ja848dOSdb6y2pSuXLfxI3qYiI9mt/V1L2rYuuSXd13SoXyUUouepqcpQnhesoxZrP4unrtXfxj/tjXdZ3Hal9fCen0mU/8Pru7GlxH/V+opjc14dc6+5c3/nhJFvDi3LT9kxv7/VHiWpzX/fE698dI+kw1dF1r7RuhXfLTam3V2WRntzC/e/uVzlHmOOF8ilMvP0i3LMz89r3weHqbV5qxGvbGu8x57+bz1MJx10Y2f7SOsirPH9fd+9498nmiJjJET329lpicUzXtr7O78U86e7H4es6lT+so/df6J/oejiPyT/zl4+F/PX/AIVc7rb26Tp0H/WqLptPzsnc3B/nhmeWdY8ceyf5b4I/25Z9W4+kOK5v/wCSPPt6kckCyMSULarnF8MtE6RMbeg0fKXujYq6MaspE6auT8Q0tJP0K3hlKnptnHr7Ywa4pc/PXq9Jp4ScYpJNyawk+cHRpEzEQ5WS0RaZ8mv8T9BushGcINyh5ilhtf3MuM4W94iax1hr6P47HS01tPSXkFobs47c8+MbWcnwcnbll3PHx63zQ+j/AARopVVLd/U+XHjhfL9GfQcBimmPVnynpbNXJknl7O11nVrbh5j7t5R6cn4a9Xh4aszbbxk9fW5bc5T9X8/ocLLNebo+w4Xm5NW9S3TWbZY9CsPQ25z9UWQRmBXZIJUyZA+fHhbrdLd27IWJZcJxmk/D2vOP2JidTtExuJh7uj/9GX49O17wtT/Zx/7nVr6T86uJb0LH6b/Rf0b410kIOFiti3dfPOxSjiy6c14efEl6E4OPx1rq0T3n6ztXivRmW9uasx2iPlEQv698SaO/RaiFd0XOVTUYyjOEm8rhZRfiOLxXxWis9WfCcDnxZ62tXpD5icV9E29b1K26NcbJuSprVdaaitsFjjhc+Fy/kXtktbUTPZnTFSkzNY1vrJrepW3RrjZNyjTDZUmorbDjjhc+Fy/kLZLWiItPZNMVKTM1jW+srLusXzdrlPPfhCFv3YLdGGNq4XGNq8fImctp31790RhpGtR27fFTDXWKtVKTUI3d5JYTVu3G9S85x7lee2uXfTunkrzc2uutfB0ZfFOrypKcVP8AFOFNMZ2cNffe373n1+vk1nicnff/AL2so4XFrWunvnp7vJr6Hrd1MO3FwlXu3Ku6uu6EZ/8AFFSTw/oVpmtWNR2+a98NLzzT39kzH8K/8Wv761PcbuTyptRbXGMYxjGOMYwR4t+bn31T4NOTk108mKeq3wd0ozaeojONzxF71J5l6ceXyvmIyWjcxPfuTipPLEx+Xt7F2i67fTBQg68QbdbnTVOdbby3CUllE0zWrGo/iFb4KXnc7+ctGOokrFbl71PfufL35zuefPJnud7a8sa16m7p+uXwnbNSjLvycro2V1zrslnOXBrHl+iNIzXiZnffuytgpMRGu3b1aauu1tl83ZbJym0llpJJJYSSXCXsilrTady0pStI1WGuVWZQGdwAD0XSrcxXsb17Iju9DpGmkv1+hpDSEeq6LuVte3AmNwzs8fGcq5OMlymZxaazphfHt6fodrm47EnJYzKfEYc/uzpcNabTHLDjcXWKxPPPT6y+m6boeolUrI2c48V2yhL9OEe6+fHvlvHxcanD5Jr4mPWvL1uLqtNNzxbKbw+YzlLPt5Nq46z2nopOaYjrHVVfrY1p48x/j5lrZK1Vpgtk+LzHXeu5WVhxz/S+Tm8TxXTo7PBcDrpLyMr8y3cL6fU5E23O30GOnLXTt0anMU/U1iR0arsottDKmNhKY2KZzA8KeJuAAAAAAAAAAAAAAAAAAAAAAAJxRI3dBqNrx6P+S9bKy9R0/ULj+5rC9Zdiu5LHh84ef7F9plpdY6FG9b4YU18vUi1dspjTjdL1k9LNwsTSbXPpleMmvD5/CnUufxnCeNHR9G6P8X0woWZZm5YUozTaXvD5cM6NslL6tNnB/wAXLj3WtZcLqXxjCTblPLjdJw8bnVh8Nrgp/l0p2bV9G5L9/XH1eW1/xBvk3Hw/z4PHl4zmno6eD0fyxES4V1rk8v8AQ8NrTPd06UiqvcQ0dbp0vu4NadlJdSiwvtVnvcjYm5kimUyNjxx5GwAAAAAAAAAAAAAAAAAAAAABkCUfckSaCGxRq5R8MtFtDoVdcklhl4udW/pvieUP0wWjIiVOv693ctxjz7cickSryuNbYpehnMraQVZGk7TVROkbZ7BOjayGmEQbdDTrBeENnJKFSt5AtdpOxW5kDzB5mgAAAAAAAAAAAAAAAAAAAAAAAygL4yRZCxV5JGFWgEqgMqoIXwpLRA2Y6dE6EuyiRNVIIZUUSJRwgI2WjY1+5yV2LO6TsVyuI2OKYrgAAAAAAAAAAAAAAAAAAAAAAABbUyYRK9y9F6lhmIQlX/P8AWuPH6EicHh5JFvdASvGxjvDYj3hsRleNiqVxGxFTIEt4EHMDQKLAAAAAAAAAAAAAAAAAAAAAAAABOsmBapEoWbwJKY2MqwnYy7hs0i7RsR7hGxnuDYw7AIOYGNwBSAzuAw5Aa5VIAAAAAAAAAAAAAAAAAAAAAAAASiwJJk7DcNjO8bDeEG8bSbhsY3DYbhsHIbGNwDIDcA3BBuCUCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/2Q==" alt="">
                            <h3 class="text-center">  Windows 10 + (<?php echo $i+1; ?>)</h3>
                            <div class="d-flex justify-content-center">
                                <button class="btn btn-primary my-2">Yêu thích</button>  
                            </div>
                        </div>
                    </div>
                <?php } else {?>
                    <div class="col-12 col-md-3 py-3">
                        <div class="border">
                            <img class="w-100 hq" src="https://cdn.tgdd.vn//GameApp/-1//2-800x500-1.png" alt="">
                            <h3 class="text-center"> Windows 11 + (<?php echo $i+1; ?>)</h3>
                            <div class="d-flex justify-content-center">
                                <button class="btn btn-primary my-2">Yêu thích</button>  
                            </div>
                        </div>
                    </div>
                <?php }?>
            <?php } ?>
        </div>
    </div>
    <!-- Bài 4.3 -->
    <?php
        $font = $background = $color = $width = $font =""; 
        if(isset($_POST['submit'])){
            $background = $_POST['background'];
            $color = $_POST['color'];
            $width = $_POST['width'];
            $font = $_POST['font'];
        }
    ?>
    <div class="container pb-5">
        <h3 class="text-center pb-5"> Tạo Forn xử lý color background width text</h3> 
        <div class="row">
            <div class="col-12 col-md-5">
                <form action="" method="post">
                    <p>Màu chữ</p>
                    <input class="form-control form-control-color" value="#000000" type="color" name="color">
                    <p>Màu nền</p>
                    <input class="form-control form-control-color" value="#FFFFFF" type="color" name="background">
                    <p>Chiều rộng</p>
                    <input type="text" name="width">
                    <p>Nội dung</p>
                    <textarea name="font" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    <input class="btn btn-success my-2" type="submit" value="hiển thị" name="submit">
                </form> 
            </div>
            <div class="col-12 col-md-7">
                <div style="width:<?=$width;?> ; background:<?= $background;?>" class="content border">
                    <p style="color:<?= $color; ?>"> <?= $font; ?></p>
                </div>
            </div>
        </div>
    </div>
    <!-- bài 4.4 -->
    <?php
        $month ='';
        $Year ='';
        if(isset($_POST['guidi'])){
            $month = $_POST['month'];
            $Year = $_POST['year'];
        }
    ?>
    <div class="container">
        <h3 class="text-center">Tạo form nhập ngày tháng hiện tại</h3>
        <div class="row">
            <div class="col-12 col-md-3">
                <form action="" method="post">
                    <p>Tháng</p>
                    <input type="number"  name="month" min="1" max="12">
                    <p>Năm</p>
                    <input type="number" name="year">
                    <p>Hiển thị</p>
                    <input class="btn btn-success my-2" type="submit" name="guidi" value="Hiển thị">
                </form>
            </div>
            <div class="col-12 col-md-9">
                <h3>
                    <?= 'tháng :'.$month; ?>
                </h3>
                <h3><?= 'Năm :'.$Year; ?></h3>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
