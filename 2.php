<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="main.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <?php
        $list = [
            [
                'images' => 'https://tourhalong.com.vn/wp-content/uploads/2021/10/ha-long-tu-tren-cao.jpg',
                'title' => 'Hình ảnh 1',
                'conten' => 'Text goes here..'
            ],
            [
                'images'=> 'https://file1.dangcongsan.vn/data/0/images/2020/07/06/havtcd/halongtitc.jpg?dpi=150&quality=100&w=680',
                'title'=> 'Hình ảnh 2',
                'conten' => 'Text goes here..'
            ],
            [
                'images'=> 'https://file1.dangcongsan.vn/DATA/0/2019/07/ha_long-16_34_13_315.jpg',
                'title'=> 'Hình ảnh 3',
                'conten' => 'Text goes here..'
            ],
            [
                'images'=> 'https://hoidulich.net/wp-content/uploads/2019/09/dia-diem-dep-o-vinh-ha-long-1.jpg',
                'title'=> 'Hình ảnh 4',
                'conten' => 'Text goes here..'
            ],
            [
                'images'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShp07Vr_xCcTYO_BmAnzSyxekqSSZMUKh1nVNrK-kLQvGEr5-UuOu_Ihz8X226L4-6kBE&usqp=CAU',
                'title'=> 'Hình ảnh 5',
                'conten' => 'Text goes here..'
            ],
            [
                'images'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4M41UF-15050vio6dmffo8adhHBSnMkTsYWyHfdJS6cmopP2UQwK4jyxzyy94tAFqNO8&usqp=CAU',
                'title'=> 'Hình ảnh 6',
                'conten' => 'Text goes here..'
            ],
        ];  
    ?>
  <div class="container my-5">
        <div class="row">
            <?php
            $index = 0;
                foreach($list as $list_unit){   
            $index ++;
            ?>
                <?php 
                    if ( $index % 2 == 0) {
                ?>
                    <div class="col-12 d-flex flex-row-reverse bd-highlight align-items-center p-0 bg-primary text-white">
                        <img class="images-bf" src="<?php echo $list_unit['images'] ?>" alt="" >
                        <div class="text-conten pe-3">
                            <h3><?php echo $list_unit['title'] ?></h3>
                            <p><?php echo $list_unit['conten'] ?></p>
                        </div>
                    </div>
                <?php }
                    else{
                ?>
                    <div class="col-12 d-flex align-items-center p-0">
                        <img class="images-bf" src="<?php echo $list_unit['images'] ?>" alt="" >
                        <div class="text-conten ps-3">
                            <h3><?php echo $list_unit['title'] ?></h3>
                            <p><?php echo $list_unit['conten'] ?></p>
                        </div>
                    </div>
                <?php
                } 
                ?>
            <?php
            }
            ?>
            <!-- baid 2.3 -->
            <div class="pt-5">
                <?php
                    for($i = 1 ;$i <=6 ;$i++){
                ?>
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Item <?php echo $i; ?>
                            <span class="badge bg-secondary text-white rounded-pill"><?php echo $i; ?></span>
                        </li>
                    </ul>
                <?php
                    }
                ?>
            </div>
            <div class="pt-5">
                <div class="row">
                    <?php
                    for ($i = 0; $i <3; $i++) {
                    ?>
                        <div class="col-12 col-md-4 text-center">
                            <?php for($j = 0; $j<6 ;$j++){ ?>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" 
                                    <?php
                                        if(($i==0 && $j >= 3 ) || ($i == 1 && $j % 2 !=0) || ($i == 2 && ($j == 0 || $j== 4))){
                                            echo 'checked="checked"';
                                        } 
                                    ?>
                                    >
                                        <label class="form-check-label" for="flexCheckChecked">
                                            Checkbox <?php echo $j+1; ?>
                                        </label>
                                    </div>
                            <?php }?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>


            <?php
                $list = [
                    [
                        "image" => "https://media.worldnomads.com/explore/vietnam/halong-bay-vietnam-from-above-gettyimages.jpg",
                        "title" => "Hình ảnh 1"
                    ],
                    [
                        "image" => "https://www.visithalongbay.com/media/cache/1a/f5/1af56f83eb2ba43b31351ae2e0cb1b7b.jpg",
                        "title" => "Hình ảnh 2"
                    ],
                    [
                        "image" => "https://bcp.cdnchinhphu.vn/Uploaded_VGP/phamvanthua/20190725/0052583f41011fc2c2f516e5b4b693ec%20(1).jpg",
                        "title" => "Hình ảnh 3"
                    ],
                    [
                        "image" => "https://vcdn-english.vnecdn.net/2019/07/24/10dayVietnam4-1563961581-4041-1563961635_1200x0.jpg",
                        "title" => "Hình ảnh 4"
                    ]
                ]
            ?>
            <div class="container">
                <div class="row">
                    <?php
                    foreach ($list as $item) {
                        ?>
                    <div class="col">
                            <div class="card" style="width: 18rem;">
                                <img src="<?php echo $item['image']; ?>" style="height:200px" class="card-img-top" 
                                alt="<?php echo $item['title']; ?>">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $item['title']; ?></h5>
                                    <a href="javascript:void(0)" 
                                    onclick="openModal('<?php echo $item['image']; ?>',
                                    '<?php echo $item['title']; ?>')" class="btn btn-primary">View</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"><?php echo $list[0]['title']; ?></h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" style="overflow:hidden">
                            <img class="modal-img" src="<?php echo $list[0]['image']; ?>" style="width:100%"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                const myModal = new bootstrap.Modal(document.getElementById('myModal'), {
                    keyboard: true
                })

                function openModal(imagePath,title){
                    document.querySelector('#myModal .modal-img').setAttribute('src',imagePath);
                    document.querySelector('#myModal .modal-title').innerText=title;
                    myModal.show();
                }
            </script>
<!--link bts  -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>