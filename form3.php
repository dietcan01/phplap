<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <meta name="viewport" content="width=i, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- bài 5.1 -->
    <?php
        $name = $status = "";
        $errorname = $erroestatus ="";
        // validate
        if($_SERVER["REQUEST_METHOD"] = "POST"){
            if(empty($_POST['name'])){
                $errorname = "<span style='color:red;'>Error: Không được để trống</span>";
            }else{
                $name = $_POST['name'];
                if(!preg_match("/^[a-zA-Z ]*$/",$name)){
                    $errorname = "<span style='color:red;'>Error: bạn chỉ viết tối đa 30 ký tự</span>";
                }
            }
        }

        if($_SERVER["REQUEST_METHOD"] = "POST"){
            if(empty($_POST['status'])){
                $erroestatus = "<span style='color:red;'>Error: Không được để trống</span>";
            }else{
                $status = $_POST['status'];
            }
        }
    ?>
    <div class="container">
        <h3 class="text-center">Tạo form status</h3>
        <form action="" method="post">
            <div class="row">
                <div class="col-12 col-md-4">
                    <p class="m-0">Name:</p>
                    <input type="text" name="name"> </br>
                    <span><?php echo $errorname ; ?></span>
                    <p class="m-0">Trạng thái</p>
                    <input type="text" name="status"></br>
                    <span><?php echo $erroestatus ; ?></span>
                    <p></p>
                    <input type="submit" name="submit" value="Chọn" id="">
                </div>
                <div class="col-12 col-md-8 border">
                    <p> name:<?php echo $name; ?></p>
                    <p> Trạng thái:<?php echo $status; ?></p>
                </div> 
            </div>
        </form>
    </div>
    <!-- bài 5.2 -->
    <?php  
        $img = $title = $price = $seo = $error = "";
        if(isset($_POST['chon'])){
            $img = $_POST['img'];
            $title = $_POST['title'];
            $price  = $_POST['price'];
            $seo = $_POST['seo'];
        }
    ?>
        <div class="container">
            <h3 class="text-center py-3">Tạo sản phẩm bằng form</h3>
            <div class="row"> 
                <div class="col-12 col-md-5">
                    <form action="" method="post">
                        <p class="m-0">ảnh sản phẩm</p>
                        <input type="text" name="img"> </br>
                        <p class="m-0">title sản phẩm</p>
                        <input type="text" name="title"></br>
                        <p class="m-0">giá sản phẩm</p>
                        <input type="text" name="price"></br>
                        <p class="m-0">giá seo sản phẩm</p> 
                        <input type="text" name="seo"></br>
                        <p></p>
                        <input type="submit" name="chon">
                    </form>
                </div>
                <div class="col-12 col-md-7">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="border">
                                <img  class="w-100" src="<?php echo $img; ?>" alt="">
                                <div class="conten text-center pt-2  pb-3">
                                    <h4 ><?php echo $title; ?></h4>
                                    <span class="px-3">
                                        <?php if($seo > $price){
                                            echo "<span style='color:red;'>Error:Gía seo > price</span>";
                                        }else{
                                            echo $price;
                                        } ?>
                                    </span>
                                    <span> <?php if($seo > $price){
                                            echo "<span style='color:red;'>Error:Gía seo > price</span>";
                                        }else{
                                            echo $seo;
                                        } ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- bai 5.3 -->
        <?php
        //Sử dụng if ($_SERVER["REQUEST_METHOD"] == "POST") (hoặc GET) để kiểm tra có tồn tại phương thức POST (hoặc GET) hay không.
        //Ứng với mỗi thành phần của form ta sử dụng if(empty($_POST["giá_trị_name"])) (hoặc GET), để xác định $_POST có nội dung hay chưa?, 
        //nếu chưa có nội dung thì xuất hiện câu thông báo tương ứng, nếu đã được điền thì cho qua.

        $full_name = $email = $phone = $facebook = $birthday = "";
        $errorfull_name = $erroremail = $errorphone = $errorfacebook = $errorfacebook = "";

        // kiểm tra full_name
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if(empty($_POST['full_name'])){
                $errorfull_name = "<span style='color:red;'>Error: Họ và tên bắt buộc phải nhập.</span>";
            }else{
                $full_name = $_POST['full_name'];
                 // Kiểm tra và chỉ cho phép nhập chữ và khoảng trắng 
                 if (!preg_match("/^[a-zA-Z ]*$/",$full_name )) {
                    $errorfull_name = "<span style='color:red;'>Error: Bạn chỉ được nhập chữ cái và khoảng trắng</span>";
                }
            }
        }
        // kiểm tra email
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            if(empty($_POST['email'])){
                $erroremail = "<span style='color:red;'>Error: Email bắt buộc phải nhập.</span>";
            }else{
                $email = $_POST['email'];
                if(!preg_match("/^[A-Za-z0-9_.]{6,32}@([a-zA-Z0-9]{2,12})(.[a-zA-Z]{2,12})+$/",$email)){
                    $erroremail  = "<span style='color:red;'>Error: Email của bạn nhập chưa đúng</span>";
                }
            }
        }
        // kiểm phone 
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            if(empty($_POST['phone'])){
                $errorphone = "<span style='color:red;'>Error: Phone bắt buộc phải nhập </span>";
            }else{
                $email = $_POST['phone'];
                if(!preg_match("/[0-9]*/",$phone)){
                    $errorphone  = "<span style='color:red;'>Error: Bắt buộc phải nhập số 03 08 ... </span>";
                }
            }
        }
        // kiểm tra ngày sinh nhâtj
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            if(empty($_POST['birthday'])){
                $errorbirthday  = "<span style='color:red;'>Error: birthday bắt buộc phải nhập </span>";
            }else{
                $birthday = $_POST['birthday'];
                if(!preg_match("/^(?:\d{4}\/(?:(?:(?:(?:0[13578]|1[02])\/(?:0[1-9]|[1-2][0-9]|3[01]))|(?:(?:0[469]|11)\/(?:0[1-9]|[1-2][0-9]|30))|(?:02\/(?:0[1-9]|1[0-9]|2[0-8]))))|(?:(?:\d{2}(?:0[48]|[2468][048]|[13579][26]))|(?:(?:[02468][048])|[13579][26])00)\/02\/29)$/",$birthday)){
                    $errorbirthday   = "<span style='color:red;'>Error: bạn phải nhập năm tháng ngày! </span>";
                }
            }
        }
    ?>
    <?php echo $full_name; ?>
    <h3 class="text-center">Form đăng ký cho người dùng</h3>
    <div class="container">
        <form action="" method="post" class="form">
            <div class="validate">
                <p class="m-0 py-1">Tên người dùng</p>
                <input class="mb-2 iunput" type="text" name="full_name" placeholder="Họ và tên">
                <span><?php echo $errorfull_name ;?></span>
                <p class="m-0 py-1">Email của bạn</p>
                <input class="mb-2 iunput" type="email" name="email" placeholder="Email của bạn">
                <span><?php echo $erroremail; ?></span>
                <p class="m-0 py-1">Số điện thoại của bạn</p>
                <input class="mb-2 iunput" type="text" name="phone" placeholder="Số điện thoại của bạn">
                <span><?php echo $errorphone; ?></span>
                <p class="m-0 py-1">facebook của bạn</p>
                <input class="mb-2 iunput" type="text" name="facebook" placeholder="facebook của bạn.">
                <span><?php echo $errorfacebook; ?></span>
                <p class="m-0 py-1">Ngày sinh nhật của bạn</p>
                <input class="mb-2 iunput" type="text" name="birthday" placeholder="Ngày sinh nhật của bạn">
                <span><?php echo $errorbirthday; ?></span>
                <p class="m-0 py-1"> Giới tính</p>
                <input type="radio" name="nam" value="1">
                <label>Nam</label>
                <input type="radio" name="nu" value="1">
                <label>Nữ</label>
                <input class="" type="submit" value="Đăng ký">
            </div>
        </form>
    </div>
    </body>
</html>