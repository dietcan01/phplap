<!-- createTable.php -->
<?php
    // kết nối database
    include "data.php";

    // Tạo bảng trong cơ sỏ dữ liệu 
    // UNSIGNED AUTO_INCREMENT PRIMARY KEY: Tự động gán id cho người dùng
    $sql = "CREATE TABLE hoidongquantri(
        id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        taikhoan VARCHAR(30) NOT NULL,
        matkhau VARCHAR(30) NOT NULL,
        level INT(6)
    )";
    // THỰC THI TRUY VẤN
    if($conn ->query($sql) == TRUE){
        echo"<script> alert('Tạo bảng thành công'); </script>";
    }else{
        echo"<script> alert('Tạo bảng thất bại'); </script>";
    }
?>