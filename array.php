<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="main.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <!-- Hàm count() sẽ đếm số phần tử trong mảng. Hàm sẽ trả về số nguyên là số phần tử trong mảng. -->
    <div class="container my-5">

            <!-- Bài 3.1 -->

        <table class="table table-bordered table-hover ">
            <thead>
                <tr>
                    <th>Key</th>
                    <th>Value</th>
                </tr>
            </thead>
            <tbody>
              <?php
                $array = [4,6,8,9,11,2];
                $tong= 0;
              ?>
              <?php
                for($i=0; $i < count($array);$i++){
                    $tong +=$array[$i]; 
              ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $array[$i];?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td >Todal</td>
                    <td><?php echo $tong ?></td>
                </tr>
            </tbody>
        </table>

        <!-- bài 3.2 -->

        <?php
            $accounts = [
                ['name' => 'Nguyễn Huy Hoàng','email' => 'hoannh@gmail.com','phone'=>'0986523648'],
                ['name' => 'Trần  Đình Hậu','email' => 'hautd@gmail.com','phone'=>'0987456158'],
                ['name' => 'Võ Hoài Nam','email' => 'namvh158@gmail.com','phone'=>'098412575'],
                ['name' => 'Cao Mai Linh','email' => 'linhcao@gmail.com','phone'=>'0965478245'],
                ['name' => 'Hỗ Ngọc Hòa','email' => 'hoahng@gmail.com','phone'=>'0965236897'],
                ['name' => 'Cao Sỹ Phúc','email' => 'phuccsao@gmail.com','phone'=>'0912548796'],
                ['name' => 'Mai Như Quỳnh','email' => 'quynhcm@gmail.com','phone'=>'0912045587'],
                ['name' => 'Trần Tiến Đạt','email' => 'dattt@gmail.com','phone'=>'0943898989']
            ];
        ?>
        <table class="table table-bordered table-hover my-5">
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Phone</td>
            </tr>
            <?php
                foreach($accounts as $listarray){
            ?>
            <tr>
                <td> <?php echo $listarray['name'] ?> </td>
                <td><?php echo $listarray['email'] ?></td>
                <td><?php echo $listarray['phone'] ?></td>
            </tr>
            <?php
                }
            ?>
        </table>

        <!-- bài 3.3 -->
        <?php
            $list = [
                ['name' => 'Nguyễn Huy Hoàng','email' => 'hoannh@gmail.com','phone'=>'0986523648','avatar'=>'https://delmonteassistedliving.com/wp-content/uploads/2021/07/testi-1-1.jpg','gende'=>0],
                ['name' => 'Trần  Đình Hậu','email' => 'hautd@gmail.com','phone'=>'0987456158','avatar'=>'https://delmonteassistedliving.com/wp-content/uploads/2021/07/testi-1-1.jpg','gende'=>0],
                ['name' => 'Võ Thị Hoài','email' => 'hoaivh158@gmail.com','phone'=>'098412575','avatar'=> 'https://verbalearn.com/wp-content/uploads/2021/01/1-anh-dai-dien-la-gi.png','gende'=>1],
                ['name' => 'Cao Mai Linh','email' => 'linhcao@gmail.com','phone'=>'0965478245','avatar'=> 'https://verbalearn.com/wp-content/uploads/2021/01/1-anh-dai-dien-la-gi.png','gende'=>1],
                ['name' => 'Hỗ Ngọc Hòa','email' => 'hoahng@gmail.com','phone'=>'0965236897','avatar'=> 'https://verbalearn.com/wp-content/uploads/2021/01/1-anh-dai-dien-la-gi.png','gende'=>1],
                ['name' => 'Cao Sỹ Phúc','email' => 'phuccsao@gmail.com','phone'=>'0912548796','avatar'=>'https://delmonteassistedliving.com/wp-content/uploads/2021/07/testi-1-1.jpg','gende'=>0],
                ['name' => 'Mai Như Quỳnh','email' => 'quynhcm@gmail.com','phone'=>'0912045587','avatar'=> 'https://verbalearn.com/wp-content/uploads/2021/01/1-anh-dai-dien-la-gi.png','gende'=>1],
                ['name' => 'Trần Tiến Đạt','email' => 'dattt@gmail.com','phone'=>'0943898989','avatar'=>'https://delmonteassistedliving.com/wp-content/uploads/2021/07/testi-1-1.jpg','gende'=>0]
            ];
        ?>
          <table class="table table-bordered table-hover my-5">
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Phone</td>
                <td>Gende</td>
                <td>Avatar</td>
            </tr>
            <?php
                foreach($list as $array){
            ?>
            <tr>
                <td> <?php echo $array['name'] ?> </td>
                <td><?php echo $array['email'] ?></td>
                <td><?php echo $array['phone'] ?></td>
                <td>
                    <?php  
                        if($array['gende'] == 0){
                            echo '<h5 class="text-danger"> Nam </h5>';
                        }else{
                            echo '<h5 class="text-danger"> Nữ </h5>';
                        }
                    ?>
                </td>
                <td class="text-center"><img class="avatar-img" src="<?php echo $array['avatar'] ?>" alt="ảnh "></td>
            </tr>
            <?php
                }
            ?>
        </table>

        <!-- Bài 3.4 -->
            <?php
                $product = [
                    ['name'=> 'Rau củ sạch 1','images'=> 'https://img1.kienthucvui.vn/uploads/2019/10/30/anh-chup-rau-cu-qua_112150407.jpg','price'=>1,'sale_price'=>1],
                    ['name'=> 'Rau củ sạch 2','images'=> 'https://raucuquagiasi.com/thumbs/533x400x2/upload/product/cung-cap-rau-xanh-gia-si-tphcm-9272.jpg','price'=>1,'sale_price'=>1],
                    ['name'=> 'Rau củ sạch 3','images'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFDF4dFyfOf5hkjNi8BBRi-d2FStPNQWZVtA&usqp=CAU','price'=>0,'sale_price'=>0],
                    ['name'=> 'Hoa quả sạch','images'=> 'https://kinhtechungkhoan.vn/stores/news_dataimages/thuuyen/092019/10/16/in_article/5918_hoaqua.jpg','price'=>1,'sale_price'=>1]
                ];
            ?>
            <div class="row">
                <?php
                    foreach($product as $list){
                ?>
                    <div class="col-12 col-md-3">
                        <div class="border"> 
                            <img class="w-100 hq" src="<?php echo $list['images'] ?>" alt="">
                            <div class="conten">
                                <h4 class="text-center py-2"><?php echo $list['name'] ?></h4>
                                <p>
                                    <?php
                                        if($list['price'] == 0 && $list['sale_price'] == 0 ){
                                            echo '<p class="text-center pb-3"> price: 100.000đ </p>';
                                        }else {
                                            echo '<div class="d-flex justify-content-center"> 
                                                    <p class="px-2"> old: 100.000đ </p>
                                                    <p> price: 100.000đ </p>
                                                </div>';
                                        }
                                    ?>
                                </p>
                                <div class="pb-2 d-flex justify-content-center">
                                    <button type="button" class="btn btn-primary mx-3">Primary</button>
                                    <button type="button" class="btn btn-danger">Danger</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    }
                ?>
            </div>

            <!-- BÀI 3.4 -->
            <?php
                $productList = [
                    ['name'=> 'Rau củ sạch 1','images'=> 'https://img1.kienthucvui.vn/uploads/2019/10/30/anh-chup-rau-cu-qua_112150407.jpg','price'=>'150000','quantity'=>2,'id'=>1],
                    ['name'=> 'Rau củ sạch 2','images'=> 'https://raucuquagiasi.com/thumbs/533x400x2/upload/product/cung-cap-rau-xanh-gia-si-tphcm-9272.jpg','price'=>'180000','quantity'=>3,'id'=>2],
                    ['name'=> 'Rau củ sạch 3','images'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFDF4dFyfOf5hkjNi8BBRi-d2FStPNQWZVtA&usqp=CAU','price'=>'100000','quantity'=>5,'id'=>3],
                    ['name'=> 'Hoa quả sạch','images'=> 'https://kinhtechungkhoan.vn/stores/news_dataimages/thuuyen/092019/10/16/in_article/5918_hoaqua.jpg','price'=>'120000','quantity'=>9,'id'=>4]
                ];
            ?>
            <table class="table table-bordered table-hover my-5">
                <tr>
                    <td>ID</td>
                    <td>Images</td>
                    <td>Name</td>
                    <td>price</td>
                    <td>quantity</td>
                    <td>Sub Total</td>
                </tr>
                <?php
                    $SubTotal =0;
                    $tolalQuantity = 0;
                    $tolalPrice = 0;
                    function currency_format($number, $suffix = 'đ') {
                        if (!empty($number)) {
                            return number_format($number, 0, ',', '.') . "{$suffix}";
                        }
                    }
                    foreach($productList as $arrayProduct){
                        $SubTotal = ($arrayProduct['price'])*($arrayProduct['quantity']);
                        $tolalQuantity +=$arrayProduct['quantity'];
                        $tolalPrice +=$SubTotal;
                ?>
                    <tr>
                        <td><?php echo $arrayProduct['id'] ;?></td>
                        <td><img class="product-img" src="<?php echo $arrayProduct['images'] ?>" alt=""></td>
                        <td><?php echo $arrayProduct['name'] ;?></td>
                        <td><?php echo currency_format($arrayProduct['price']);?></td>
                        <td><?php echo $arrayProduct['quantity']; ?></td>
                        <td><?php echo currency_format($SubTotal);?></td>
                    </tr>
                <?php
                    }
                ?>
            </table>
            <table class="table table-bordered table-hover my-5">
                <tr>
                    <td>Tolal quantity</td>
                    <td><?php echo $tolalQuantity;?></td>
                </tr>
                <tr>
                    <td>Tolal price</td>
                    <td><?php echo currency_format($tolalPrice); ?></td>
                </tr>
            </table>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>