<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="main.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- bài 1.1 -->
    <?php
        $name = 'Nguyễn Huy Hoàng';
        $age = 25;
        $phone = '0986421127';
        $email = 'luongitvnsoft@gmail.com';
        $address = 'Số 7 - Mỹ Đình - Cầu giấy - Hà Nội';
    ?>
    <table border="1">
        <tr>
            <td>
                Name
            </td>
            <td>
                <?php echo $name; ?>
            </td>
        </tr>

        <tr>
            <td>
                Age
            </td>
            <td>
                <?php echo $age; ?>
            </td>
        </tr>

        <tr>
            <td>
                Phone
            </td>
            <td>
                <?php echo $phone; ?>
            </td>
        </tr>

        <tr>
            <td>
                Email
            </td>
            <td>
                <?php echo $email; ?>
            </td>
        </tr>

        <tr>
            <td>
                Address
            </td>
            <td>
                <?php echo $address; ?>
            </td>
        </tr>
    </table>

    <!-- bài 1.2 Khai báo 2 biến $first_name và $last_name, sử dụng dấu . để nối giá trị 2 biến để in ra tên đầy đủ -->
    <?php
       $first_name = 'Nguyễn Huy';
       $last_name = 'Hoàng';
       echo $first_name.$last_name;
    ?>

    <!--bài 1.3  -->
    <?php
        $name = 'Nguyễn Huy Hoàng';
        $age = 25;
        $phone = '0986421127';
        $email = 'luongitvnsoft@gmail.com';
        $address = 'Số 7 - Mỹ Đình - Cầu giấy - Hà Nội';
        $avatar = 'https://delmonteassistedliving.com/wp-content/uploads/2021/07/testi-1-1.jpg';
        $gende = 1;
    ?>
      <table border="1">
        <tr>
            <td>
                Name
            </td>
            <td>
                <?php echo $name; ?>
            </td>
        </tr>

        <tr>
            <td>
                Age
            </td>
            <td>
                <?php echo $age; ?>
            </td>
        </tr>

        <tr>
            <td>
                Phone
            </td>
            <td>
                <?php echo $phone; ?>
            </td>
        </tr>

        <tr>
            <td>
                Email
            </td>
            <td>
                <?php echo $email; ?>
            </td>
        </tr>

        <tr>
            <td>
                Address
            </td>
            <td>
                <?php echo $address; ?>
            </td>
        </tr>

        <tr>
            <td>
                Avatar
            </td>
            <td>
                <img class="image" src="<?php echo $avatar; ?>" alt="">
            </td>
        </tr>

        <tr>
            <td>
                Gende
            </td>
            <td>
                <input <?php if($gende == 1){ echo 'checked="checked"';} ?> type="radio" value="1"/>
                <label>Nam</label>

                <input <?php if($gende == 0){ echo 'checked="checked"';} ?> type="radio" value="0"/>
                <label>Nữ</label>
            </td>
        </tr>

    </table>
</body>
</html>