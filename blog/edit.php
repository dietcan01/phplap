<?php
    include"database.php";
    $db = new Database();
?>
<?php
    $id = $_GET['this_id'];

    $db->select("SELECT * FROM listblog WHERE id ='$id'");

    $row = $db->fetch();

    $test = $row['vitri'];
    $status = $row['trangthai'];

    $check = explode(",",$test);
?>
<?php $db->edit($id) ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <!-- header -->
    <?php include "header.php" ?>
    <!-- end header -->
    <div class="dear">
        <div class="row">
            <div class="col-2">
                <div class="list d-flex align-items-center px-5">
                    <div class="list-icon pt-3">
                        <span class="px-3"><i class="fa-solid fa-align-justify"></i></span>
                        <span class="px-3 "><a class="text-decoration-none text-dark" href="index.php">List</a> </span>
                    </div>
                </div>
                <div class="list d-flex align-items-center px-5">
                    <div class="list-icon py-3">
                        <span class="px-3"><i class="fa-regular fa-square-plus"></i></span>
                        <span class="px-3 "><a class="text-decoration-none text-dark" href="add.php"> Edit</a></span>
                    </div>
                </div>
                <div class="list d-flex align-items-center px-5">
                    <div class="list-icon ">
                        <span class="px-3"><i class="fa-solid fa-magnifying-glass"></i></span>
                        <span class="px-3 "><a class="text-decoration-none text-dark" href="search.php"> Search</a></span>
                    </div>
                </div>
            </div>

            <div class="col-10 bg-dear">
                <div class=" mt-4 me-4 ms-2 mb-3 information">
                    <label class="d-flex p-2 fw-bold bg-list text-list border" for="">Edit Blog</label>
                    <form class="p-3" action="" method="post">
                        <div class="my-3">
                            <label for="formGroupExampleInput" class="form-label">Tiêu đề:</label>
                            <input type="text" name="edittitle" class="form-control py-2" value="<?php echo $row['tin'] ?>" id="formGroupExampleInput">
                        </div>
                        <label class="pt-1 pb-2" for="">Mô tả ngắn:</label>
                        <div class="form-floating my-2">
                            <textarea name="editdescription" class="form-control " placeholder="Leave a comment here" id="floatingTextarea"><?=$row['motangan']?></textarea>
                            <label for="floatingTextarea2">Comments</label>
                        </div>
                        <label class="pt-1 pb-2" for="">Chi tiết:</label>
                        <div class="form-floating my-2">
                            <textarea name="editdetail" class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"><?=$row['chitiet']?></textarea>
                            <label for="floatingTextarea2">Comments</label>
                        </div>
                        <label class="pt-1 pb-2" for="">Hình ảnh:</label>
                        <div class="my-2">
                           <input type="file"> 
                        </div>
                        <label class="pt-1 pb-2" for="">Vị trí:</label>
                        <div class="my-2">
                            <input type="checkbox" name='editlocation[]' value="Việt Nam" 
                                <?php 
                                    if(in_array("Việt Nam", $check)){
                                        echo "checked";
                                    }
                                ?>> Việt Nam 
                               
                                
                            <input class="mx-2" type="checkbox" name='editlocation[]'  value="Châu Á" 
                                <?php 
                                    if(in_array("Châu Á", $check)){
                                        echo "checked";
                                    }
                                ?>
                                > Châu Á 
                                
                            <input class="mx-2" type="checkbox" name='editlocation[]' value="Châu Âu"
                                <?php 
                                    if(in_array("Châu Âu", $check)){
                                        echo "checked";
                                    }
                                ?>> Châu Âu 
                               
                            <input class="mx-2" type="checkbox" name='editlocation[]' value="Châu Mỹ" 
                                <?php 
                                    if(in_array("Châu Mỹ", $check)){
                                        echo "checked";
                                    }
                                ?>> Châu Mỹ 
                                
                        </div>
                        <label class="pt-1 pb-2" for="">Public:</label>
                        <div>
                        <input type="radio" id="yes" name="editstatus" value="Yes" <?php if( $status == "Yes"){ echo "checked";}?> >
                            <label for="html">Yes</label>
                        <input type="radio" id="no" name="editstatus"  value="No" <?php if( $status == "No"){ echo "checked";}?> >
                            <label for="css">No</label>
                        </div>
                        <label class="pt-1 pb-2" for=""></label>
                        <div class="row my-2">
                            <div class="col">
                            <label class="pb-2" for="">Loại:</label>
                                <select name="edittype" class="form-select" aria-label="Default select example">
                                    <option selected><?php echo $row['loai'] ?></option>
                                    <option value="Kinh doanh">Kinh doanh</option>
                                    <option value="Gỉa trí">Gỉa trí</option>
                                    <option value="Thế Giới">Thế Giới</option>
                                    <option value="Thời Sự">Thời Sự</option>
                                </select>
                            </div>
                            <div class="col">
                            <label class="pb-2" for="">Date public:</label>
                                <input type="date" name="editdate" class="form-control" placeholder="Last name" aria-label="Last name" value="<?php echo $row['thoigian'] ?>">
                            </div>
                        </div>
                        <div class="bg-list py-3 mt-5 d-flex justify-content-center">
                            <input class="btn btn-success" type="submit" name="edit" value="submit">
                            <a class="btn btn-primary mx-3" href="">Clear</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>   
    <!-- footer -->
    <?php include "foote.php" ?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>