<?php
    include"database.php";
    $db = new Database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>

<?php 
    $db->add(); 
?>
    <!-- header -->
    <?php include "header.php" ?>
    <!-- end header -->
    <div class="dear">
        <div class="row">
            <div class="col-2">
                <div class="list d-flex align-items-center px-5">
                    <div class="list-icon pt-3">
                        <span class="px-3"><i class="fa-solid fa-align-justify"></i></span>
                        <span class="px-3 "><a class="text-decoration-none text-dark" href="index.php">List</a> </span>
                        
                    </div>
                </div>
                <div class="list d-flex align-items-center px-5">
                    <div class="list-icon py-3">
                        <span class="px-3"><i class="fa-regular fa-square-plus"></i></span>
                        <span class="px-3 "><a class="text-decoration-none text-dark" href="add.php"> News</a></span>
                    </div>
                </div>
                <div class="list d-flex align-items-center px-5">
                    <div class="list-icon ">
                        <span class="px-3"><i class="fa-solid fa-magnifying-glass"></i></span>
                        <span class="px-3 "><a class="text-decoration-none text-dark" href="search.php"> Search</a></span>
                    </div>
                </div>
            </div>
            <div class="col-10 bg-dear">
                <div class=" mt-4 me-4 ms-2 mb-3 information">
                    <label class="d-flex p-2 fw-bold bg-list text-list border" for="">New Blog</label>
                    <form class="p-3" action="" method="post">
                        <div class="my-2">
                            <label for="formGroupExampleInput" class="form-label">Tiêu đề:</label>
                            <input type="text" name="title" class="form-control py-2" id="formGroupExampleInput">
                        </div>
                        <label class="pt-3 " for="">Mô tả ngắn:</label>
                        <div class="form-floating py-2">
                            <textarea name="description" class="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                            <label for="floatingTextarea2">Comments</label>
                        </div>
                        <label class="pt-3" for="">Chi tiết:</label>
                        <div class="form-floating py-2">
                            <textarea name="detail" class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
                            <label for="floatingTextarea2">Comments</label>
                        </div>
                        <label class="pt-3" for="">Hình ảnh:</label>
                        <div class="my-2">
                           <input type="file"> 
                        </div>
                        <label class="pt-3" for="">Vị trí:</label>
                        <div class="my-2">
                            <input type="checkbox" name='location[]' value="Việt Nam "> Việt Nam 
                            <input class="mx-2" type="checkbox" name='location[]' value="Châu Á "> Châu Á 
                            <input class="mx-2" type="checkbox" name='location[]' value="Châu Âu "> Châu Âu 
                            <input class="mx-2" type="checkbox" name='location[]' value="Châu Mỹ"> Châu Mỹ 
                        </div>
                        <label class="pt-3 pb-2" for="">Public:</label>
                        <div>
                        <input type="radio" id="yes" name="status" value="Yes">
                            <label for="html">Yes</label>
                        <input type="radio" id="no" name="status" value="No">
                            <label for="css">No</label>
                        </div>
                        <label class="pt-1" for=""></label>
                        <div class="row my-2">
                            <div class="col">
                            <label class="pb-2" for="">Loại:</label>
                                <select name="type" class="form-select" aria-label="Default select example">
                                    <option selected>----Choose---</option>
                                    <option value="Kinh doanh">Kinh doanh</option>
                                    <option value="Gỉa trí">Gỉa trí</option>
                                    <option value="Thế Giới">Thế Giới</option>
                                    <option value="Thời Sự">Thời Sự</option>
                                </select>
                            </div>
                            <div class="col">
                            <label class="pb-2" for="">Date public:</label>
                                <input type="date" name="date" class="form-control" placeholder="Last name" aria-label="Last name">
                            </div>
                        </div>
                        <div class="bg-list py-3 mt-5 d-flex justify-content-center">
                            <input class="btn btn-success" type="submit" name="submit" value="submit">
                            <a class="btn btn-primary mx-3" href="">Clear</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>   
    <!-- footer -->
    <?php include "foote.php" ?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
