<?php
    class Database {
        // Khai bao thuộc tính
       private $server = 'localhost';
       private $username = 'root';
       private $password = '';
       private $database = 'blog';

       private $conn = NULL;
       private $result = NULL;

        // xây dựng phương thức    
       public function connect(){
            $this->conn = new mysqli($this->server,$this->username,$this->password,$this->database)
            or die('kết nối thất bại');
            $this->conn->query("SET NAMES 'utf8'");
       }
      
        // phương thức select dữ liệu  
        public function select($sql){
            // thực thi câu lệnh sql
            $this->connect();
            $this->result = $this->conn->query($sql);
        }

        // lấy dữ liệu
        public function fetch(){
            // 
            if($this->result->num_rows > 0){
                $row = $this->result->fetch_assoc();
            }else{
                $row = 0;
            }
            return $row;
        }
        // phương thức chung  cho inser,update,delete
        public function comnand($sql){
            $this->connect();
            $this->conn->query($sql);
        }
        
        // phương thức thêm
        public function add() {
            if(isset($_POST['submit'])){

                if(isset($_POST['title'])){
                    $title = $_POST['title'];
                }
            
                if(isset($_POST['description'])){
                    $description=$_POST['description'];
                }
                
                if(isset($_POST['detail'])){
                    $detail = $_POST['detail'];
                }
               
                $location="";
                if(isset($_POST['location'])){
                    $location = implode(',',$_POST['location']);
                }

                if(isset($_POST['date'])){
                    $date = $_POST['date'];
                }
               
                $status = "";
                if(isset($_POST['status']) ){
                   $status = $_POST['status']; 
                }
                
                if(isset($_POST['type'])){
                   $type = $_POST['type']; 
                }
    
                $this->comnand("INSERT INTO listblog (tin,loai,trangthai,vitri,thoigian,motangan,chitiet)
                VALUES('$title','$type','$status','$location','$date','$description ','$detail ')"); 
                header('Location: index.php');
            }
        }


        // phương thức xóa
        public function delete($this_id) {
            $this_id = $_GET['this_id'];
            echo " ID " .$this_id.".";
        
            $this->comnand("DELETE FROM listblog WHERE id = $this_id");
            header('Location: index.php');
        }

        // PHƯƠNG THỨC SEARCH
        public function search($search){
            if(isset($_POST['Search'])){
                $search = $_POST['searchbar'];
                $this->select("SELECT * FROM listblog WHERE tin LIKE '%$search%'");
            }else{

                $this->select("SELECT * FROM listblog");
            }
        }


        // phương thức edit
        public function edit($id){
            if(isset($_POST['edit'])){

                if(isset($_POST['edittitle'])){
                    $title = $_POST['edittitle'];
                }
            
                if(isset($_POST['editdescription'])){
                    $description=$_POST['editdescription'];
                }
                
                if(isset($_POST['editdetail'])){
                    $detail = $_POST['editdetail'];
                }
               
                $location="";
                if(isset($_POST['editlocation'])){
                    $location = implode(',',$_POST['editlocation']);
                }

                if(isset($_POST['editdate'])){
                    $date = $_POST['editdate'];
                }
               
                $status = "";
                if(isset($_POST['editstatus']) ){
                   $status = $_POST['editstatus']; 
                }
                
                if(isset($_POST['edittype'])){
                   $type = $_POST['edittype']; 
                }
    
                $this->comnand("UPDATE listblog SET tin ='$title',loai='$type',trangthai=' $status',vitri='$location',thoigian='$date',motangan='$description',chitiet='$detail' WHERE id ='$id'");
                header('Location: index.php');
            }
        }


        // edit
    }
?>