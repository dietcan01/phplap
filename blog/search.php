<?php
    include"database.php";
    $db = new Database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
 
    <!-- header -->
    <?php include "header.php" ?>
    <!-- end header -->
    <div class="dear">
        <div class="row">
            <div class="col-2">
                <div class="list d-flex align-items-center px-5">
                    <div class="list-icon pt-3">
                        <span class="px-3"><i class="fa-solid fa-align-justify"></i></span>
                        <span class="px-3 "><a class="text-decoration-none text-dark" href="index.php">List</a> </span>
                    </div>
                </div>
                <div class="list d-flex align-items-center px-5">
                    <div class="list-icon py-3">
                        <span class="px-3"><i class="fa-regular fa-square-plus"></i></span>
                        <span class="px-3 "><a class="text-decoration-none text-dark" href="add.php"> News</a></span>
                    </div>
                </div>
                <div class="list d-flex align-items-center px-5">
                    <div class="list-icon ">
                        <span class="px-3"><i class="fa-solid fa-magnifying-glass"></i></span>
                        <span class="px-3 "><a class="text-decoration-none text-dark" href="search.php"> Search</a></span>
                    </div>
                </div>
            </div>
            <div class="col-10 bg-dear">
                <div class=" mt-4 me-4 ms-2 mb-3 text-center">
                <form class="d-flex my-5" method="post">
                    <input class="form-control me-2 py-2" name="searchbar" type="search" placeholder="Search" aria-label="Search">
                    <input class="btn btn-outline-success" type="submit" name="Search" id="" value="Search"> 
                </form>
                    <table class="table table-bordered border information m-0">
                        <thead>
                            <label class="d-flex p-2 fw-bold bg-list text-list" for="">Search Blog</label>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">Tin</th>
                                <th scope="col">Loại</th>
                                <th scope="col">Trạng thái</th>
                                <th scope="col">Vị trí</th>
                                <th scope="col">Ngày public</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <!-- Lấy dữ kiệu mysql -->
                        <?php
                            $search ='';
                            $db->search($search);
                            while( $row = $db->fetch()){
                        ?>
                        <tbody>
                            <tr>
                                <th scope="row"><?= $row['id'] ?></th>
                                <td><?= $row['tin'] ?></td>
                                <td><?= $row['loai'] ?></td>
                                <td><?= $row['trangthai'] ?></td>
                                <td><?= $row['vitri'] ?></td>
                                <td><?= $row['thoigian'] ?></td>
                                <td><a class="btn btn-outline-primary" href="edit.php?this_id=<?= $row['id'] ?>">Edit</a></td>
                                <td><a class="btn btn-outline-danger" href="delete.php?this_id=<?= $row['id'] ?>">Delete</a></td>
                            </tr>
                        </tbody>
                        <?php }?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->
    <?php include "foote.php" ?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>